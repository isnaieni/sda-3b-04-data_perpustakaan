from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from PIL import ImageTk, Image
import sqlite3
import data as data

root = Tk()
root.title('MyLibrary')
root.iconbitmap('bookicon.ico')
root.geometry("846x640")
root.resizable(width=False, height=False)

conn = sqlite3.connect('data.db')
cs = conn.cursor()

def button_book():
    global search_book

    book_menu.pack(fill='both', expand=1)
    main_menu.forget()
    book_history.forget()

    canvas = Canvas(book_menu, width= 800, height= 600)
    canvas.grid(columnspan=6, rowspan=22)

    label_book = Label(book_menu, image=logo_book)
    label_book.grid(column=2, row=7, columnspan=2)

    search_book = Entry(book_menu, width=40, font=('Helvetica', 12))
    search_book.grid(column=1, row=9, columnspan=4)

    button_book = Button(book_menu, image=enter, borderwidth=0, command= find_book)
    button_book.grid(column=2, row=10, columnspan=2)

    button_history = Button(book_menu, image=historybutt, borderwidth=0, command= button_book_history)
    button_history.grid(column=0, row=20, columnspan=2)

    button_back = Button(book_menu, image=back, borderwidth=0, command= back_menu)
    button_back.grid(column=4, row=20, columnspan=2)

    label_down = Label(book_menu, text=" ©2021 ", width=120, height=2,fg='white', bg='#5e83a2')
    label_down.grid(column=0, row=22, columnspan=6)

def button_writer():
    global search_writer

    writer_menu.pack(fill='both', expand=1)
    main_menu.forget()
    writer_history.forget()

    canvas = Canvas(writer_menu, width= 800, height= 600)
    canvas.grid(columnspan=6, rowspan=22)

    label_writer = Label(writer_menu, image=logo_writer)
    label_writer.grid(column=2, row=7, columnspan=2)

    search_writer = Entry(writer_menu, width=40, font=('Helvetica', 12))
    search_writer.grid(column=1, row=9, columnspan=4)

    button_writer = Button(writer_menu, image=enter, borderwidth=0, command= find_writer)
    button_writer.grid(column=2, row=10, columnspan=2)

    button_history1 = Button(writer_menu, image=historybutt, borderwidth=0, command= button_writer_history)
    button_history1.grid(column=0, row=20, columnspan=2)

    button_back = Button(writer_menu, image=back, borderwidth=0, command= back_menu)
    button_back.grid(column=4, row=20, columnspan=2)

    label_down = Label(writer_menu, text=" ©2021 ", width=120, height=2,fg='white', bg='#5e83a2')
    label_down.grid(column=0, row=22, columnspan=6)

def button_book_history():
    book_menu.forget()
    book_history.pack(fill='both', expand=1)

    canvas = Canvas(book_history, width= 800, height= 600)
    canvas.grid(columnspan=6, rowspan=22)
    
    label_title = Label(book_history, text=" HISTORY ", width=120)
    label_title.grid(column=0, row=1, columnspan=6)

    table = ttk.Treeview(book_history, columns=(1,2,3), show="headings", height="20")
    table.grid(column=0, row=2, columnspan=6)

    table.column(1, width=50, anchor=CENTER)
    table.column(2, width=280)
    table.column(3, width=200, anchor=CENTER)
    # table.column(4, width=100)

    table.heading(1, text="NO")
    table.heading(2, text="NAMA BUKU")
    table.heading(3, text="WAKTU")
    # table.heading(4, text="WAKTU")

    table.grid(column=1, row=2, columnspan=4)

    cs.execute("SELECT * FROM data_buku")
    myresult = cs.fetchall()

    for i in myresult:
        table.insert('', 'end', values=i)
    
    button_delete = Button(book_history, image=delete, borderwidth=0, command= delete_book)
    button_delete.grid(column=0, row=16, columnspan=2)
    
    button_back = Button(book_history, image=back, borderwidth=0, command= button_book)
    button_back.grid(column=4, row=16, columnspan=2)

    label_down = Label(book_history, text=" ©2021 ", width=120, height=2,fg='white', bg='#5e83a2')
    label_down.grid(column=0, row=22, columnspan=6)

def button_writer_history():    
    writer_history.pack(fill='both', expand=1)
    writer_menu.forget()

    canvas = Canvas(writer_history, width= 800, height= 600)
    canvas.grid(columnspan=6, rowspan=22)
    
    label_title = Label(writer_history, text=" HISTORY ", width=120)
    label_title.grid(column=0, row=1, columnspan=6)

    table = ttk.Treeview(writer_history, columns=(1,2,3), show="headings", height="20")
    table.grid(column=1, row=2, columnspan=4)

    table.column(1, width=50, anchor=CENTER)
    table.column(2, width=280)
    table.column(3, width=200, anchor=CENTER)

    table.heading(1, text="NO")
    table.heading(2, text="NAMA PENULIS")
    table.heading(3, text="WAKTU")

    cs.execute("SELECT * FROM data_penulis")
    myresult = cs.fetchall()

    for i in myresult:
        table.insert('', 'end', values=i)

    button_delete = Button(writer_history, image=delete, borderwidth=0, command= delete_writer)
    button_delete.grid(column=0, row=16, columnspan=2)
    
    button_back = Button(writer_history, image=back, borderwidth=0, command= button_writer)
    button_back.grid(column=4, row=16, columnspan=2)

    label_down = Label(writer_history, text=" ©2021 ", width=120, height=2,fg='white', bg='#5e83a2')
    label_down.grid(column=0, row=22, columnspan=6)

def back_menu():
    book_menu.forget()
    writer_menu.forget()
    main_menu.pack(fill='both', expand=1)

def find_book():
    frame_book = LabelFrame(book_menu)
    frame_book.grid(column=1, row=12, columnspan=4)

    find = search_book.get()
    find = find.lower()

    for key in data.bookDict.keys():
        if key == find:
            messagebox.showinfo("INFO", "Buku Tersedia")
            label_recomend = Label(book_menu, text="Baca juga:", font=('Helvetica', 11))
            label_recomend.place(x=190, y=300)

            for edge in  data.bookDict[key]:
                Label(frame_book, text=edge, width=50, font=('Helvetica', 11),  anchor="w", bg='white').pack() #grid(column=2, row= 15, columnspan=2
            break
    else:
        messagebox.showerror("MAAF", "Buku Tidak Tersedia")
    
    name = find.title()
    cs.execute(""" INSERT INTO data_buku (history_buku) VALUES (?)""", (name,))
    conn.commit()

def find_writer():
    frame_writer = LabelFrame(writer_menu)
    frame_writer.grid(column=1, row=12, columnspan=4)

    find = search_writer.get()
    find = find.lower()

    for key in data.writerDict.keys():
        if key == find:
            label_recomend = Label(writer_menu, text="Rekomendasi buku dari " + find.title() + ":", font=('Helvetica', 11))
            label_recomend.place(x=190, y=300)

            for edge in  data.writerDict[key]:
                Label(frame_writer, text=edge, width=50, font=('Helvetica', 11),  anchor="w", bg='white').pack() #grid(column=2, row= 14, columnspan=2)
            break
    else:
        messagebox.showerror("maaf", "buku tidak tersedia")
    
    name = find.title()
    cs.execute(""" INSERT INTO data_penulis (history_penulis) VALUES (?)""", (name,))
    conn.commit()  

def history():
    cs.execute("SELECT * FROM history")
    myresult = cs.fetchall()
    print(myresult)

def delete_book():
    cs.execute("DROP TABLE data_buku")

def delete_writer():
    cs.execute("DROP TABLE data_penulis")

cs.execute(""" CREATE TABLE IF NOT EXISTS data_buku (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            history_buku TEXT,
            timestamp DATE DEFAULT (datetime('now','localtime'))
             )""")

cs.execute(""" CREATE TABLE IF NOT EXISTS data_penulis (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            history_penulis TEXT,
            timestamp DATE DEFAULT (datetime('now','localtime'))
            )""")

main_menu = Frame(root)
book_menu = Frame(root, width=800, height=400)
writer_menu = Frame(root)
book_history = Frame(root)
writer_history = Frame(root)

main_menu.pack(fill='both', expand=1)

canvas = Canvas(main_menu, width= 800, height= 600)
canvas.grid(columnspan=6, rowspan=22)

logo = Image.open('book.png')
resize= logo.resize((300,300), Image.ANTIALIAS)
logo = ImageTk.PhotoImage(resize)
label = Label(main_menu, image=logo)
label.grid(column=2, row=9, columnspan=2)

title = Image.open('mylibrary.png')
resize1= title.resize((360,90), Image.ANTIALIAS)
title = ImageTk.PhotoImage(resize1)
label_title = Label(main_menu, image=title)
label_title.grid(column=2, row=10, columnspan=2)

logo_book = Image.open('iconbook.png')
resize_book = logo_book.resize((70,70), Image.ANTIALIAS)
logo_book = ImageTk.PhotoImage(resize_book)

logo_writer = Image.open('iconwriter.png')
resize_writer = logo_writer.resize((70,70), Image.ANTIALIAS)
logo_writer = ImageTk.PhotoImage(resize_writer)

caribuku = ImageTk.PhotoImage(Image.open('caribuku.png'))
label_caribuku = Label(image=caribuku)

caripenulis = ImageTk.PhotoImage(Image.open('caripenulis.png'))
label_caripenulis = Label(image=caripenulis)

enter = ImageTk.PhotoImage(Image.open('enter.png'))
label_enter = Label(image=enter)

historybutt = ImageTk.PhotoImage(Image.open('history.png'))
label_history = Label(image=historybutt)

delete = ImageTk.PhotoImage(Image.open('delete.png'))
label_delete = Label(image=delete)

back = ImageTk.PhotoImage(Image.open('back.png'))
label_back = Label(image=back)

button_1 = Button(main_menu, image=caribuku, command=button_book, borderwidth=0)
button_1.grid(column=2, row=13)

button_2 = Button(main_menu, image=caripenulis, command=button_writer, borderwidth=0)
button_2.grid(column=3, row=13)

label_down = Label(main_menu, text=" ©2021 ", width=120, height=2,fg='white', bg='#5e83a2')
label_down.grid(column=0, row=22, columnspan=6)

root.mainloop()