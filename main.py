import data as data
import os
import sqlite3
from tabulate import tabulate

conn = sqlite3.connect('data.db')
cs = conn.cursor()

def findBook():
    find = input("Masukkan nama buku : ")

    cs.execute(""" INSERT INTO data_buku (history_buku) VALUES (?)""", [find])
    conn.commit()

    os.system('cls')
    find = find.lower()
    for key in data.bookDict.keys():
        if key == find:
            print("Buku", find.title(), "Tersedia")
            print("Baca juga")
            for edge in data.bookDict[key]:
                print(edge, end="")
                print("")
            break
    else:
        print("buku tidak tersedia")

def findWriter():
    find = input("Masukkan nama penulis : ")

    cs.execute(""" INSERT INTO data_penulis (history_penulis) VALUES (?)""", [find])
    conn.commit()

    os.system('cls')
    find = find.lower()
    for key in data.writerDict.keys():
        if key == find:
            print(find.title(), "Penulis Buku :")
            for edge in data.writerDict[key]:
                print(edge, end="")
                print("")
            break
    else:
        print("penulis tidak ditemukan")
    
def historyBook():
    cs.execute("SELECT * FROM data_buku")
    myresult = cs.fetchall()
    print(tabulate(myresult, headers=['ID', 'Nama_Buku'], tablefmt='psql'))

def historyWriter():
    cs.execute("SELECT * FROM data_penulis")
    myresult = cs.fetchall()
    print(tabulate(myresult, headers=['ID', 'Nama_Penulis'], tablefmt='psql'))

def welcome():
    print(f"""
    +-------------------------+
    |      SELAMAT DATANG     |
    |     INI PERPUSTAKAAN    |
    +-------------------------+
    """)
    input("Tekan Enter untuk melanjutkan...")
    os.system('cls')

def menu():
    print(f"""
    +----+---------------------+
    | NO |        MENU         |
    +----+---------------------+
    | 1. | Cari Buku           |
    | 2. | Cari Penulis        |
    +----+---------------------+
    (H)History         (x)Keluar
    """)

def menuHistory():
    print(f"""
    +----+---------------------+
    | NO |       HISTORY       |
    +----+---------------------+
    | 1. | Buku                |
    | 2. | Penulis             |
    +----+---------------------+
                       (x)Keluar
    """)

cs.execute(""" CREATE TABLE IF NOT EXISTS data_buku (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            history_buku TEXT
             )""")

cs.execute(""" CREATE TABLE IF NOT EXISTS data_penulis (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            history_penulis TEXT
            )""")

welcome()
while True:
    menu()
    choice = input("pilih : ")
    os.system('cls')
    if choice == "1":
        findBook()
        while True:
            book = input("Ingin mencari buku lagi (Y/N) : ")
            if book.upper() == "Y":
                findBook()
            elif book.upper() == "N":
                os.system('cls')
                break
            else:
                print("Maaf inputan salah!")   
    elif choice == "2":
        findWriter()
        while True:
            writer = input("Ingin mencari penulis lagi (Y/N): ")
            if writer.upper() == "Y":
                findWriter()
                while True:
                    book = input("Ingin mencari buku (Y/N) : ")
                    if book.upper() == "Y":
                        findBook()
                    elif book.upper() == "N":
                        os.system('cls')
                        break
                    else:
                        print("Maaf inputan salah!")
            elif writer.upper() == "N":
                while True:
                    book = input("Ingin mencari buku (Y/N) : ")
                    if book.upper() == "Y":
                        findBook()
                    elif book.upper() == "N":
                        os.system('cls')
                        break
                    else:
                        print("Maaf inputan salah!")
                os.system('cls')
                break
            else:
                print("Maaf inputan salah!")
    elif choice.lower() == "h":
        menuHistory()
        while True:
            user = input("pilih : ")
            os.system('cls')
            if user == "1":
                historyBook()
                print("(x)Keluar\n")
            elif user == "2":
                historyWriter()
                print("(x)Keluar\n")
            elif user.lower() == "x":
                break
            else:
                print("Maaf inputan salah!")
    elif choice.lower() == "x":
        break
    else:
        print("Inputan Salah")